from aima.search import Problem, depth_first_tree_search, breadth_first_tree_search, astar_search
import sys

# We're using a one-dimensional list of elements as a data structure.
# All together we have 15 rows: 5 horizontal rows, 5 left diagonal rows and 5 right diagonal rows.
class HexProblemPuzzle(Problem):
    actionCount = 0
    resultCount = 0
    memoryUsage = 0
    possibleElements = range(1, 20)
    horizontalStartIndexes = (0, 3, 7, 12, 16)
    leftDiagonalStartIndexes = (2, 1, 0, 3, 7)
    rightDiagonalStartIndexes = (0, 1, 2, 6, 11)

    spiralRoute = tuple([0, 1, 2, 6, 11, 15, 18, 17, 16, 12, 7, 3, 4, 5, 10, 14, 13, 8, 9])
    smallValues = {1, 2, 4, 5, 6, 7, 8}
    smallValueIndexes = (4, 5, 8, 9, 10, 13, 14)
    bigValues = set(possibleElements) - set(smallValues)
    bigValueIndexes = set(spiralRoute) - set(smallValueIndexes)

    horizontalStep = (0, 1, 1, 1, 1)
    leftDiagonalStep = (0, 4, 5, 5, 4)
    rightDiagonalStep = (0, 3, 4, 4, 3)
    sizes = (3, 4, 5, 4, 3)

    def __init__(self, initial, end):
        self.initial = tuple(initial)
        self.goal = tuple(end)

    #1)First find out all available elements (all elements - used elements
    #2)If there no elements, return empty list
    #3)If current state is already invalid, return empty list
    #4)Find the first empty cell (using spiral movement)
    #5)Specify which allowed elements could be used: for inner layer only elements under 9 can be used
    #4)Iterate over all possible remaining elements and check for each one, if it violates our rules, if not, then add to returnable list
    #5)Return all possible allowed elements
    def actions(self, state):
        availableElements = set(self.possibleElements) - set(state)
        if len(availableElements) == 0 and self.goal_test2(state, False): return []
        firstEmptyCell = self.getFirstEmptyCell(state)
        allowedElements = []
        if firstEmptyCell in self.smallValueIndexes:
            searchValues = self.smallValues - set(state)
        else :
            searchValues = self.bigValues - set(state)

        for elem in searchValues:
            localState = list(state)
            localState[firstEmptyCell] = elem
            if self.goal_test2(localState, False):
                allowedElements.append((firstEmptyCell, elem))
        self.actionCount = self.actionCount + 1
        self.memoryUsage = self.memoryUsage + sys.getsizeof(allowedElements)
        return tuple(allowedElements)

    # Go through possible indexes in a spiral way, starting from upper left corner and moving right
    def getFirstEmptyCell(self, state):
        for i in self.spiralRoute:
            if state[i] == 0:
                return i


    #Just add a value to the first empty cell
    def result(self, state, (emptyCell, value)):
        self.resultCount = self.resultCount + 1
        localState = list(state)
        localState[emptyCell] = value
        return tuple(localState)

    # For checking the solution we need to iterate over all rows and see if sum of the elements in a row is 38.
    # To do that we have three lists that contain starting indexes for all three types of rows. We also have three lists for steps (to iterate over all rows)
    # It does get trickier when we reach the middle point /the middle row, for diagonal rows we then have to change the steps
    #   (for left diagonal we have to add to a step and for the right diagonal we have to subtract it)
    # We can easily test the goal_test with the right solution

    #We also have the possibility to specify an additional parameter, that does not require explicit check. For actions we want to know whether adding an element
    #violates our conditions. This means, that when checkExact = False, then it only checks whether sum of row is > 38, if it is, then it violates our condition
    def goal_test2(self, state, checkExact=True):
        def checkRowSum(state, startIndexes, steps, checkExact=True, desiredSum=38):
            for i in range(0, 5):
                if i == 1 or i == 3 : continue
                sum = 0
                incrementIndex = 0
                if i==4: steps = ((steps[0],)+steps[3:])
                for k in range(0, self.sizes[i]):
                    sum += state[startIndexes[i] + incrementIndex + steps[k]]
                    incrementIndex += steps[k]
                    if not checkExact and sum > desiredSum: return False
                if checkExact and sum != desiredSum: return False
            return True

        if checkRowSum(state, self.horizontalStartIndexes, self.horizontalStep, checkExact) == False :
            return False
        if checkRowSum(state, self.leftDiagonalStartIndexes, self.leftDiagonalStep, checkExact) == False :
            return False
        if checkRowSum(state, self.rightDiagonalStartIndexes, self.rightDiagonalStep, checkExact) == False :
            return False
        return True

    def goal_test(self, state):
       return self.goal_test2(state)

    def h(self, node): return 5*38 - sum(node.state)

p = HexProblemPuzzle([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [])

from time import gmtime, strftime
print strftime("%Y-%m-%d %H:%M:%S", gmtime())
print(astar_search(p))
print p.actionCount
print p.resultCount
print p.memoryUsage
print strftime("%Y-%m-%d %H:%M:%S", gmtime())
