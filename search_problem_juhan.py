
import aima.search

#ab = aima.search.GraphProblem('A', 'B', aima.search.romania)
#print(aima.search.breadth_first_tree_search(ab).solution())
#print(aima.search.depth_first_graph_search(ab).solution())
#print(aima.search.uniform_cost_search(ab).solution())
#print(aima.search.astar_search(ab).solution())

def h0(x):
    return 0

#print(aima.search.astar_search(ab,h0).solution())

class EightPuzzle(aima.search.Problem):
    
    board=()
    goal = [1,2,3,4,5,6,7,8,0]
    
    def __init__(self, initial, goal):
        self.board = self.initial = tuple(initial)
        self.goal = tuple(goal)
        
        
    def actions(self,state):
        print "action"
        if state.index(0) == 0 :
            return ["right", "down"]
        elif state.index(0) == 1 :
            return ["left", "right", "down"]
        elif state.index(0) == 2 :
            return ["left", "down"]
        #  need more cases here

    def result(self,state,action):
        print "result"
        localstate = list(state)
        if action == "left":
            blank = state.index(0)
            localstate[blank]=localstate[blank-1]
            localstate[blank-1] = 0
            return tuple(localstate)
        if action == "right":
            blank = state.index(0)
            localstate[blank]=localstate[blank+1]
            localstate[blank+1] = 0
            return tuple(localstate)
        if action == "down":
            blank = state.index(0)
            localstate[blank]=localstate[blank+3]
            localstate[blank+3] = 0
            return tuple(localstate)


    def goal_test(self, state):
        print "goal"
        return tuple(self.goal) == tuple(state)

    def h(self, node):
        """ A misplaced tiles heuristic for 8-puzzle.
            The heuristic with the name h and argument node
            is picked up automatically by astar_search.
            It may be useful to define the heuristic function in the problem instance class
            because it may be necessary to access some additional data
            structures specific to the problem
        """
        print "heuristic"
        sum = 0
        for i in range(0,8):
            if not (self.goal[i]==node.state[i]):
                sum = sum + 1
        return sum

#print(aima.search.breadth_first_search(EightPuzzle([1,0,2,3,4,5,6,7,8],[0,1,2,3,4,5,6,7,8])))
        

print(aima.search.astar_search(EightPuzzle([1,0,2,3,4,5,6,7,8],[0,1,2,3,4,5,6,7,8])))
           
        
    










doc="""
>>> ab = GraphProblem('A', 'B', romania)
>>> breadth_first_tree_search(ab).solution()
['S', 'F', 'B']
>>> breadth_first_search(ab).solution()
['S', 'F', 'B']
>>> uniform_cost_search(ab).solution()
['S', 'R', 'P', 'B']
>>> depth_first_graph_search(ab).solution()
['T', 'L', 'M', 'D', 'C', 'P', 'B']
>>> iterative_deepening_search(ab).solution()
['S', 'F', 'B']
>>> len(depth_limited_search(ab).solution())
50
>>> astar_search(ab).solution()
['S', 'R', 'P', 'B']
>>> recursive_best_first_search(ab).solution()
['S', 'R', 'P', 'B']
"""

